public class QuestionTest {
        private final String question;
        private final String correctAnswer;
        private final String userAnswer;

        public QuestionTest(String question, String rightQuestion, String userQuestion) {
            this.question = question;
            this.correctAnswer = rightQuestion;
            this.userAnswer = userQuestion;
        }

        public String getQuestion() {
            return question;
        }

        public String getCorrectAnswer() {
            return correctAnswer;
        }

        public String getUserAnswer() {
            return userAnswer;
        }

        @Override
        public String toString() {
            String result = "";
            result += "Вопрос: " + question + "; ";
            result += "Ваш ответ: " + correctAnswer;
            if (correctAnswer.equals(userAnswer)) {
                result += "; Вы дали верный ответ!";
            } else {
                result += "; Верный ответ: " + correctAnswer;
            }
            return result;
        }
    }

