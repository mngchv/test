import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class QIn {

    public static void QuestionTest() throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.print("Введите количество вопросов : ");
        int countOfQuestions = scanner.nextInt();
        System.out.print("Введите количество ответов на один вопрос : ");
        int countOfAnswers = scanner.nextInt();
        int countOfCorrect = 0;
        int totalCost = 0;
        int countForTest;
        System.out.print("Введите стоимость вопроса (балл) : ");
        int cost = scanner.nextInt();
        String[] QQ = new String[countOfQuestions];
        String[] wrongA = new String[countOfAnswers*countOfQuestions-countOfQuestions];
        String[] rightA = new String[countOfQuestions];

        for (int i = 0; i < QQ.length; i++) {
            System.out.print("Введите " + (i + 1) + " вопрос : ");
            QQ[i] = scanner.next();
            for (int j = 0; j < countOfAnswers - 1; j++) {
                System.out.print("Введите " + (j + 1) + " неверный ответ на " + (i + 1) + " вопрос : ");
                wrongA[j + i * (countOfAnswers - 1)] = scanner.next();
            }
            System.out.print("Введите верный ответ на " + (i + 1) + " вопрос : ");
            rightA[i] = scanner.next();
        }

        System.out.println("Введите количество вопросов, необходимых для тестирования");
        countForTest = scanner.nextInt();
        if (countOfQuestions < countForTest) {
            System.exit(0);
        }
        System.out.println("");
        Instant starts = Instant.now();
        Thread.sleep(10);

        ArrayList<QuestionTest> testedQuestions = new ArrayList<>();

        ArrayList<Integer> numbersForReduction = new ArrayList<>();
        while (numbersForReduction.size() < countForTest) {
            int number = random.nextInt(countOfQuestions);
            if (!numbersForReduction.contains(number)) {
                numbersForReduction.add(number);
            }
        }

        for (int i = 0; i < countForTest; i++) {
            int questionIndex = numbersForReduction.get(i);
            String question = QQ[questionIndex];

            System.out.println(question);

            ArrayList<String> variantsPull = new ArrayList<>(countOfAnswers);
            String correctAnswer = rightA[questionIndex];
            variantsPull.add(correctAnswer);
            for (int j = 0; j < countOfAnswers - 1; j++) {
                variantsPull.add(wrongA[questionIndex * (countOfAnswers - 1) + j]);
            }

            while (variantsPull.size() > 0) {
                Random r = new Random();
                int randInd = r.nextInt(variantsPull.size());
                String randomVariant = variantsPull.get(randInd);
                variantsPull.remove(randInd);
                System.out.println(randomVariant);
            }

            System.out.println("");
            System.out.print("Введите ответ : ");
            String answer = scanner.next();

            QuestionTest testQuestion = new QuestionTest(
                    question,
                    correctAnswer,
                    answer
            );
            testedQuestions.add(testQuestion);
            if (answer.equalsIgnoreCase(rightA[questionIndex])) {
                countOfCorrect++;
                totalCost += cost;
            }

        }
        Instant ends = Instant.now();
        System.out.println("На выполнение теста вам понадобилось " + Duration.between(starts, ends).getSeconds() + " секунд");

        for (QuestionTest questionTest : testedQuestions) {
            System.out.println(questionTest);
        }

        System.out.println("Вы заработали " + totalCost + " баллов");
        System.out.println("Правильных ответов : " + countOfCorrect);
    }
}